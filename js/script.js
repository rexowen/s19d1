let movies = [
		        {
		        title: "Avengers 1",
		        genre: "Sci-Fi, Drama, Action",
		        releasedDate: Date(2000, 4, 1),
		        rating: 5,
		        displayRating: function(){
		        console.log(`The movie  ${this.title} has ${this.rating} Stars \n `);
		        },
		        displayDetails: function(){
		        console.log(`${this.title} is an ${this.genre} movie`);
		        }
		        },
		        {
		        title: "Avengers 2",
		        genre: "Sci-Fi, Thriller",
		        releasedDate: Date(2005, 4, 2),
		        rating: 6,
		        displayRating: function(){
		        console.log(`The movie  ${this.title} has ${this.rating} Stars \n `);
		        },
		        displayDetails: function(){
		        console.log(`${this.title} is an ${this.genre} movie`);
		        }
		        },
		        {
		        title: "Avengers 3",
		        genre: "Sci-Fi, Action, Drama",
		        releasedDate: Date(2010, 4, 3),
		        rating: 7,
		        displayRating: function(){
		        console.log(`The movie  ${this.title} has ${this.rating} Stars \n `);
		        },
		        displayDetails: function(){
		        console.log(`${this.title} is an ${this.genre} movie`);
		        }
		        },
		        {
		        title: "Avengers 4",
		        genre: "Sci-Fi, Adventure",
		        releasedDate: Date(2015, 4, 4),
		        rating: 8,
		        displayRating: function(){
		        console.log(`The movie  ${this.title} has ${this.rating} Stars \n `);
		        },
		        displayDetails: function(){
		        console.log(`${this.title} is an ${this.genre} movie`);
		        }
		        },
		        {
		        title: "Avengers 5",
		        genre: "Sci-Fi, Action, Drama",
		        releasedDate:  Date(2021, 4, 5),
		        rating: 9,
		        displayRating: function(){
		        console.log(`The movie  ${this.title} has ${this.rating} Stars \n `);
		        },
		        displayDetails: function(){
		        console.log(`${this.title} is an ${this.genre} movie`);
		        }
		        },
		]

showAllMovies = () => {
	for(let i=0; i < movies.length; i++){
		console.log(`${movies[i].title} ${movies[i].genre}`)
	}
}
showAllMovies();

showTitles = (num) => {
	for(let i=0; i < movies.length; i++){
		console.log(`${movies[i].title} has ${movies[i].rating} stars.`);
	}if (movies.rating == 2) {
		console.log(`No movies with that rating.`);
	}
}

showTitles(4);